package history

import (
	"bytes"
	"encoding/csv"
	"io"
	"strings"

	"bitbucket.org/powerdesigninc/pdi-bitrise-database/utils"
)

const installFlow = "install"
const fileFlow = "file"

type History struct {
	Repo  string
	Flow  string
	Items map[string]*HistoryItem
}

type HistoryItem struct {
	Id      string
	File    string
	Hash    string
	Updated bool
}

func LoadInstallHistory(files []*utils.FileInfo) (*History, error) {
	var repo = utils.GetRepoName()
	var sql *bytes.Buffer
	var err error

	if len(files) > 0 {
		var builder = strings.Builder{}
		for _, f := range files {
			if builder.Len() != 0 {
				builder.WriteString(",")
			}

			builder.WriteString("'" + f.Filename + "'")
		}

		sql, err = utils.FormatByMapT(fetchHistoryTemplate, map[string]interface{}{
			"Repo":  repo,
			"Flow":  installFlow,
			"Files": builder.String(),
		})

		if err != nil {
			return nil, err
		}
	}

	return loadHistory(repo, installFlow, sql)
}

func LoadFileHistory() (*History, error) {
	var repo = utils.GetRepoName()
	var sql, err = utils.FormatByMapT(fetchHistoryTemplate, map[string]interface{}{
		"Repo": repo,
		"Flow": fileFlow,
	})

	if err != nil {
		return nil, err
	}

	return loadHistory(repo, fileFlow, sql)
}

func LoadUnpublishedInstall() (*History, error) {
	var repo = utils.GetRepoName()
	var sql, err = utils.FormatByMapT(fetchUnpublishedInstallTemplate, map[string]interface{}{
		"Repo": repo,
	})

	if err != nil {
		return nil, err
	}

	return loadHistory(repo, installFlow, sql)
}

func (h *History) CheckHash(file string, hash string) bool {
	item, ok := h.Items[file]
	if ok {
		return item.Hash == hash
	} else {
		return false
	}
}

func (h *History) SetHash(file string, hash string) {
	item, ok := h.Items[file]
	if ok {
		item.Hash = hash
		item.Updated = true
	} else {
		h.Items[file] = &HistoryItem{
			File:    file,
			Hash:    hash,
			Updated: true,
		}
	}
}

func (h *History) Save() error {
	var buffer = &bytes.Buffer{}

	for _, item := range h.Items {
		if item.Updated {
			temp, _ := utils.FormatByMapT(insertUpdateHistoryTemplate, map[string]interface{}{
				"Repo": h.Repo,
				"Flow": h.Flow,
				"File": item.File,
				"Hash": item.Hash,
			})

			buffer.Write(temp.Bytes())
		}
	}

	if buffer.Len() == 0 {
		return nil
	}

	buffer.WriteString("\nCOMMIT;")

	err := utils.ExecuteSqlE(buffer.Bytes())

	if err != nil {
		utils.PrintWarning("Save History Failed, Deatil:", err.Error())
		return utils.ErrNoPrintError
	}

	return nil
}

func (h *History) Publish() error {
	buffer, _ := utils.FormatByMapT(updatePublishTemplate, map[string]interface{}{
		"Repo": h.Repo,
		"Flow": installFlow,
	})

	err := utils.ExecuteSqlE(buffer.Bytes())

	if err != nil {
		utils.PrintWarning("Publish History Failed, Deatil:", err.Error())
		return utils.ErrNoPrintError
	}

	return nil
}

func loadHistory(repo string, flow string, sql *bytes.Buffer) (*History, error) {
	var history = &History{
		Repo:  repo,
		Flow:  flow,
		Items: map[string]*HistoryItem{},
	}

	if sql != nil {
		output, err := utils.ExecuteSqlOutput(sql.Bytes())
		if err != nil {
			utils.PrintWarning("Load Histroy Failed, Deatil:", output, "\n", err.Error())
			return nil, utils.ErrNoPrintError
		}

		csvReader := csv.NewReader(strings.NewReader(output))

		for {
			line, err := csvReader.Read()
			if err == io.EOF {
				break
			} else if err != nil {
				return nil, err
			}

			if line[0] == "ID" {
				// header
				continue
			}

			history.Items[line[1]] = &HistoryItem{
				Id:   line[0],
				File: line[1],
				Hash: line[2],
			}
		}
	}

	return history, nil
}
