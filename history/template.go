package history

import "text/template"

var fetchHistoryTemplate, _ = template.New("FetchRecentHistory").Parse(`
SET MARKUP CSV ON;
SET FEEDBACK OFF;

SELECT
  id,
  filename,
  hash
FROM
  apps.xxcicd_histories
WHERE
    repo = '{{.Repo}}'
  AND flow = '{{.Flow}}'{{if .Files}} AND filename in ({{.Files}}){{end}};`)

var fetchUnpublishedInstallTemplate, _ = template.New("FetchUnpublishedInstall").Parse(`
SET MARKUP CSV ON;
SET FEEDBACK OFF;

SELECT
  id,
  filename,
  hash
FROM
  apps.xxcicd_histories
WHERE
    repo = '{{.Repo}}'
  AND flow = 'install'
	AND publish_yn = 'N';`)

var insertUpdateHistoryTemplate, _ = template.New("InsertUpdateHistory").Parse(`
MERGE INTO apps.xxcicd_histories
USING dual ON ( repo = '{{.Repo}}'
                AND flow = '{{.Flow}}'
                AND filename = '{{.File}}' )
WHEN MATCHED THEN UPDATE
SET hash = '{{.Hash}}',
    update_date = sysdate
WHEN NOT MATCHED THEN
INSERT (
  repo,
  flow,
  filename,
  hash,
  update_date )
VALUES
  ( '{{.Repo}}',
  '{{.Flow}}',
  '{{.File}}',
  '{{.Hash}}',
  sysdate );`)

var updatePublishTemplate, _ = template.New("UpdatePublish").Parse(`
UPDATE apps.xxcicd_histories
SET publish_yn = 'Y',
    update_date = sysdate
WHERE
  repo = '{{.Repo}}'
  AND flow = '{{.Flow}}'
  AND publish_yn = 'N';

COMMIT;`)
