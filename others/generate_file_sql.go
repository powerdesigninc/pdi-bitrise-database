package main

import (
	"fmt"
	"io/ioutil"
	"mime"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	file := os.Args[1]
	apexFilePath := os.Args[2]
	appID := os.Args[3]

	fileBytes, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}

	var fileMime = mime.TypeByExtension(filepath.Ext(file))
	var blobBuilder = strings.Builder{}

	var tableIndex = 1
	var length = len(fileBytes)

	var eof = false

	for start := 0; !eof; {
		end := start + 200
		if end >= length {
			end = length
			eof = true
		}

		blobBuilder.WriteString(fmt.Sprintf("\twwv_flow_api.g_varchar2_table(%v) := '%X';\n", tableIndex, fileBytes[start:end]))

		tableIndex++
		start = end
	}

	var sql = fmt.Sprintf(`
DECLARE
	l_workspace_id number;
	l_appId number := %s;
BEGIN
	SELECT workspace_id
	INTO l_workspace_id
	FROM apex_applications
	WHERE application_id = l_appId;

	apex_util.set_security_group_id (p_security_group_id => l_workspace_id);

	wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
%s

	wwv_flow_api.create_app_static_file (
		p_flow_id      => l_appId,
		p_file_name    => '%s',
		p_mime_type    => '%s',
		p_file_content => wwv_flow_api.varchar2_to_blob(wwv_flow_api.g_varchar2_table)
	);
	
	COMMIT;
END;
/`, appID, blobBuilder.String(), apexFilePath, fileMime)

	ioutil.WriteFile("temp.sql", []byte(sql), 0644)
}
