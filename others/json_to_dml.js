var repo = process.argv[2];
var flow = process.argv[3];
var filepath = process.argv[4];
if (!filepath) {
  console.log("No file specified");
  process.exit(1);
}

var json = require(filepath);

for (const key in json) {
  const hash = json[key];

  console.log(`INSERT INTO apps.xxcicd_histories ( repo, flow, filename, hash, update_date ) VALUES ( '${repo}', '${flow}', '${key}', '${hash}', sysdate );`);
}
