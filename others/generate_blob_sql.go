package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	file := os.Args[1]

	fileBytes, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}

	var blobBuilder = strings.Builder{}

	var tableIndex = 1
	var length = len(fileBytes)

	var eof = false

	for start := 0; !eof; {
		end := start + 200
		if end >= length {
			end = length
			eof = true
		}

		blobBuilder.WriteString(fmt.Sprintf("  wwv_flow_api.g_varchar2_table(%v) := '%X';\n", tableIndex, fileBytes[start:end]))

		tableIndex++
		start = end
	}

	var sql = fmt.Sprintf(`
DECLARE
  l_blob blob;
BEGIN
  wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
%s

  l_blob := wwv_flow_api.varchar2_to_blob(wwv_flow_api.g_varchar2_table);

  UPDATE xx SET xx = l_blob WHERE xx = xx;
  
  COMMIT;
END;
/`, blobBuilder.String())

	ioutil.WriteFile("./temp.sql", []byte(sql), 0644)
}
