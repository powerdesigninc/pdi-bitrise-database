package flows

import (
	changeRequestFlow "bitbucket.org/powerdesigninc/pdi-bitrise-database/flows/change-request"
	cleanFlow "bitbucket.org/powerdesigninc/pdi-bitrise-database/flows/clean"
	filesFlow "bitbucket.org/powerdesigninc/pdi-bitrise-database/flows/files"
	files2Flow "bitbucket.org/powerdesigninc/pdi-bitrise-database/flows/files2"
	installsFlow "bitbucket.org/powerdesigninc/pdi-bitrise-database/flows/installs"
	notifyFlow "bitbucket.org/powerdesigninc/pdi-bitrise-database/flows/notify"
	publishFlow "bitbucket.org/powerdesigninc/pdi-bitrise-database/flows/publish"
	qaFlow "bitbucket.org/powerdesigninc/pdi-bitrise-database/flows/qa"
	transitionFlow "bitbucket.org/powerdesigninc/pdi-bitrise-database/flows/trainsition"
)

type flow interface {
	Do() error
}

var Flows map[string]flow

func init() {
	Flows = map[string]flow{
		"clean":      cleanFlow.Flow{},
		"cr":         changeRequestFlow.Flow{},
		"files":      filesFlow.Flow{},
		"files2":     files2Flow.Flow{},
		"installs":   installsFlow.Flow{},
		"notify":     notifyFlow.Flow{},
		"publish":    publishFlow.Flow{},
		"qa":         qaFlow.Flow{},
		"transition": transitionFlow.Flow{},
	}
}
