package transitionFlow

import (
	"fmt"

	"bitbucket.org/powerdesigninc/pdi-bitrise-database/utils"
)

type Flow struct{}

var issueStatuses = []string{
	// "SPRINT BACKLOG",
	// "PICKED UP",
	// "TO DO",
	// "IN PROGRESS",
	"DONE",
	"READY FOR REVIEW",
	"REQUIRES PREPRODUCTION REVIEW",
	"READY FOR PRODUCTION",
	"IN PRODUCTION",
	"RELEASED TO USERS",
}

var prodNextStates = []string{
	"IN PRODUCTION",
}

var nonProdNextStates = []string{
	"READY FOR REVIEW",
	"REQUIRES PREPRODUCTION REVIEW",
}

// transition jira status
func (Flow) Do() error {
	tasks := utils.ReadExecutedTasks()

	if len(tasks) == 0 {
		utils.PrintWarning("No tasks to transition")
		return nil
	}

	var nextStatus []string
	if utils.IsProdBuild() {
		nextStatus = prodNextStates
	} else {
		nextStatus = nonProdNextStates
	}

	for _, task := range tasks {
		issue, err := utils.GetJiraIssue(task)
		if err == nil {
			status := issue.Status
			transition, err := utils.FindNextJiraIssueTransition(task, nextStatus)

			if err == nil {
				statusIndex := utils.IndexOfString(issueStatuses, status)
				nextStatusIndex := utils.IndexOfString(issueStatuses, transition.Name)

				if statusIndex < nextStatusIndex {
					err = utils.UpdateJiraIssue(issue.Id, transition.Id)
				} else {
					fmt.Printf("- %v (%v) doesn't need to transition\n", task, status)
					continue
				}
			}

			if err == nil {
				fmt.Printf("- %v (%v) transitioned to %v\n", task, status, transition.Name)
			} else {
				utils.PrintWarning(fmt.Sprintf("- %v (%v) failed to transition, Error: %v\n", task, status, err))
			}
		}
	}

	return nil
}
