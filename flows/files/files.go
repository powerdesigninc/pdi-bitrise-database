package filesFlow

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	his "bitbucket.org/powerdesigninc/pdi-bitrise-database/history"
	"bitbucket.org/powerdesigninc/pdi-bitrise-database/utils"
	"github.com/bitrise-io/go-utils/colorstring"
)

var rootFolder = "files"
var scriptFolder = filepath.Join(rootFolder, "scripts")
var scriptDistFolder = filepath.Join(scriptFolder, "dist")
var styleFolder = filepath.Join(rootFolder, "styles")
var staticFolder = filepath.Join(rootFolder, "statics")
var cssFile = "application.css"
var blockSize = 200
var includedFiles []string
var includedScripts = false
var includedStyles = false
var history *his.History

// hashed-based upload files, scan files and upload if hash is different
type Flow struct{}

func (Flow) Do() error {
	var err error

	history, err = his.LoadFileHistory()
	if err != nil {
		return err
	}

	includedFiles = utils.GetIncludedFiles()

	for _, file := range includedFiles {
		if strings.HasPrefix(file, "scripts") {
			includedScripts = true
		} else if strings.HasPrefix(file, "styles") {
			includedStyles = true
		}
	}

	if err := uploadScripts(); err != nil {
		utils.WriteNotifyError("Upload scripts files failed")
		utils.PrintError(err)
		return utils.ErrIgnorableError
	}

	if err := uploadStyles(); err != nil {
		utils.WriteNotifyError("Upload styles file failed")
		utils.PrintError(err)
		return utils.ErrIgnorableError
	}

	if err := uploadStatics(); err != nil {
		utils.WriteNotifyError("Upload static files failed")
		utils.PrintError(err)
		return utils.ErrIgnorableError
	}

	return history.Save()
}

// build/upload javascript files
func uploadScripts() error {
	// check if there are files changed
	hash, err := utils.FolderHash(scriptFolder)

	if err != nil {
		if _, ok := err.(*os.PathError); ok {
			// no scripts folder, skip
			return nil
		}

		return err
	}

	// check files are changed
	if !includedScripts && history.CheckHash("scripts", hash) {
		return nil
	}

	// install packages
	cmd := exec.Command("npm", "install")
	cmd.Dir = scriptFolder
	result, err := cmd.CombinedOutput()

	if err != nil {
		fmt.Println(string(result))
		return err
	}

	// build
	cmd = exec.Command("npm", "run", "build")
	cmd.Dir = scriptFolder
	result, err = cmd.CombinedOutput()

	if err != nil {
		fmt.Println(string(result))
		return err
	}

	// upload
	files, err := utils.ListAllFiles(scriptDistFolder)
	if err != nil {
		return err
	}

	err = uploadFiles(files, func(name string) string {
		return strings.ReplaceAll(name, scriptDistFolder, "scripts")
	})

	history.SetHash("scripts", hash)

	return err
}

// build/upload css files
func uploadStyles() error {
	// check if there are files changed
	hash, err := utils.FolderHash(styleFolder)

	if err != nil {
		if _, ok := err.(*os.PathError); ok {
			// no styles folder, skip
			return nil
		}

		return err
	}

	// check files are changed
	if !includedStyles && history.CheckHash("styles", hash) {
		return nil
	}

	// build
	var indexFile string
	files, err := utils.ListAllFiles(styleFolder)
	if err != nil {
		return err
	}

	needFiles := []*utils.FileInfo{
		{Filename: cssFile, Fullname: filepath.Join(styleFolder, cssFile), Extension: ".css"},
		//{Filename: cssFile + ".map", Fullname: filepath.Join(styleFolder, cssFile+".map"), Extension: ".map"},
	}

	for _, file := range files {
		if file.Extension == ".map" || file.Extension == ".css" || file.Extension == ".scss" || file.Extension == ".sass" {
			if strings.HasPrefix(file.Filename, "index") {
				indexFile = file.Filename
			}

			continue
		}

		needFiles = append(needFiles, file)
	}

	if indexFile == "" {
		return errors.New("no index.s[ac]ss file")
	}

	cmd := exec.Command("npx", "sass", "--style", "compressed", indexFile, "application.css")
	cmd.Dir = styleFolder
	result, err := cmd.CombinedOutput()

	if err != nil {
		fmt.Println(string(result))
		return err
	}

	// upload
	err = uploadFiles(needFiles, func(name string) string {
		return strings.TrimPrefix(name, rootFolder+string(filepath.Separator))
	})

	history.SetHash("styles", hash)

	return err
}

func uploadStatics() error {
	files, err := utils.ListAllFiles(staticFolder)
	if err != nil {
		if _, ok := err.(*os.PathError); ok {
			// no static folder, skip
			return nil
		}

		return err
	}

	return uploadFiles(files, func(name string) string {
		return strings.TrimPrefix(name, staticFolder+string(filepath.Separator))
	})
}

func uploadFiles(files []*utils.FileInfo, rename func(string) string) error {
	var appID = utils.GetDatabaseAppId()

	for _, file := range files {
		var apexFilePath = rename(file.Fullname)

		// fmt.Println("Checking", file.Fullname, ", ", apexFilePath)
		if history.CheckHash(file.Fullname, file.Hash()) {
			// file has been upload
			// fmt.Println("-- Skipped, the same hash")
			continue
		}

		if len(includedFiles) > 0 && !utils.ContainsString(includedFiles, strings.ToLower(apexFilePath)) {
			// if includedFiles is not null, only upload files in includedFiles
			// fmt.Println("-- Skipped, not included")
			continue
		}

		var mime = file.Mime()
		var filebytes = file.Bytes()
		var blobBuilder = strings.Builder{}

		var tableIndex = 1
		var length = len(filebytes)

		var eof = false

		fmt.Println(colorstring.Green("- uploading: " + file.Fullname + ", to: " + apexFilePath))

		for start := 0; !eof; {
			end := start + blockSize
			if end >= length {
				end = length
				eof = true
			}

			blobBuilder.WriteString(fmt.Sprintf("wwv_flow_api.g_varchar2_table(%v) := '%X';\n", tableIndex, filebytes[start:end]))

			tableIndex++
			start = end
		}

		var sql = fmt.Sprintf(`
			DECLARE
				l_workspace_id number;
				l_appId number := %s;
			BEGIN
				SELECT workspace_id
				INTO l_workspace_id
				FROM apex_applications
				WHERE application_id = l_appId;

				apex_util.set_security_group_id (p_security_group_id => l_workspace_id);

				wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
				%s

				wwv_flow_api.create_app_static_file (
					p_flow_id      => l_appId,
					p_file_name    => '%s',
					p_mime_type    => '%s',
					p_file_content => wwv_flow_api.varchar2_to_blob(wwv_flow_api.g_varchar2_table)
				);
			END;
			/
			Commit;`, appID, blobBuilder.String(), apexFilePath, mime)

		if err := utils.ExecuteSqlE([]byte(sql)); err != nil {
			return err
		}

		// save only uploaded
		history.SetHash(file.Fullname, file.Hash())
	}

	return nil
}
