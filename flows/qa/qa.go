package qaFlow

import (
	"errors"
	"fmt"

	"bitbucket.org/powerdesigninc/pdi-bitrise-database/utils"
)

type Flow struct{}

type TestSuite struct {
	Id   string
	Name string
}

const giBaseUrl = "https://api.ghostinspector.com/v1"

// trigger ghost inspector test suites
func (Flow) Do() error {
	if len(utils.ReadExecutedTasks()) == 0 {
		utils.PrintWarning("Skip QA due to no executed tasks")

		return nil
	}

	folderId, err := getFolderId(utils.GetRepoName())
	if err != nil {
		return err
	}

	suites, err := listTestSuites(folderId)
	if err != nil {
		return err
	}

	if len(suites) == 0 {
		utils.PrintWarning("No test suite in the folder")

		return nil
	}

	for _, suite := range suites {
		err := triggerTestSuite(suite)
		if err != nil {
			return err
		}
	}

	return nil
}

func getFolderId(folderName string) (string, error) {
	req := &utils.Request{
		BaseUrl: giBaseUrl,
		Url:     "/folders?apiKey=" + utils.GetGhostInspectorApiKey(),
	}

	json, err := req.DoJson()
	if err != nil {
		return "", err
	}

	for _, folder := range json.Path("data").Children() {
		if folder.Path("name").Data() == folderName {
			return folder.Path("_id").Data().(string), nil
		}
	}

	return "", errors.New("Cannot find the folder for " + folderName)
}

func listTestSuites(folderId string) ([]TestSuite, error) {
	req := &utils.Request{
		BaseUrl: giBaseUrl,
		Url:     fmt.Sprintf("/folders/%v/suites?apiKey=%v", folderId, utils.GetGhostInspectorApiKey()),
	}

	json, err := req.DoJson()
	if err != nil {
		return nil, err
	}

	fmt.Println("executing: ")

	ids := make([]TestSuite, len(json.Path("data").Children()))
	for i, folder := range json.Path("data").Children() {
		ids[i] = TestSuite{
			Id:   folder.Path("_id").Data().(string),
			Name: folder.Path("name").Data().(string),
		}
	}

	return ids, nil
}

func triggerTestSuite(suite TestSuite) error {
	fmt.Println("- " + suite.Name)

	req := &utils.Request{
		BaseUrl: giBaseUrl,
		Url:     fmt.Sprintf("/suites/%v/execute?immediate=1&apiKey=%v", suite.Id, utils.GetGhostInspectorApiKey()),
	}

	_, err := req.Do()

	return err
}
