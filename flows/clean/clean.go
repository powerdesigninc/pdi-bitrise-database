package cleanFlow

import (
	"fmt"
	"math"
	"strings"
	"time"

	"bitbucket.org/powerdesigninc/pdi-bitrise-database/utils"
	"github.com/bitrise-io/go-utils/colorstring"
)

type Flow struct{}

var reservedBranches = []string{"dev", "test", "master", "main"}
var finalStates = []string{"NONE", "DONE", "RELEASED TO USERS", "IN PRODUCTION"}
var timeRange = 60.0 // 60 days

// remove old branches
func (Flow) Do() error {
	var targetBranch = utils.GetCleanBranch()

	var today = time.Now()

	if err := utils.GitCheckout(targetBranch); err != nil {
		return err
	}

	if err := utils.GitFetch(); err != nil {
		return err
	}

	branches, err := utils.GitMergedBranch(targetBranch)
	if err != nil {
		return err
	}

	if len(branches) == 0 {
		fmt.Println(colorstring.Yellowf("No merged branches"))
		return nil
	}

	for _, branchName := range branches {
		remoteIndex := strings.Index(branchName, "/")
		remote := branchName[0:remoteIndex]
		branch := branchName[remoteIndex+1:]

		if utils.ContainsString(reservedBranches, branch) {
			continue
		}

		commitDate, err := utils.GitCommitDate(branchName)
		if err != nil {
			return err
		}

		dayAgo := math.Round(today.Sub(commitDate).Hours() / 24)
		jiraId := utils.FindJiraId(branch)
		jiraStatus := "NONE"

		if jiraId != "" {
			jira, err := utils.GetJiraIssue(jiraId)

			if err == nil {
				jiraStatus = jira.Status
			}
		}

		execStatus := "Skipped"
		if utils.ContainsString(finalStates, jiraStatus) && dayAgo >= timeRange {
			if err := utils.GitDeleteBranch(remote, branch); err != nil {
				execStatus = colorstring.Yellow("Error")
			} else {
				execStatus = colorstring.Red("Removed")
			}
		}

		fmt.Printf("- %v (%v, %v days ago) : %v\n", branch, jiraStatus, dayAgo, execStatus)
	}

	return nil
}
