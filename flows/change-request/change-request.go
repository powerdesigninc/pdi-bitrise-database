package changeRequestFlow

import (
	"fmt"
	"time"

	"bitbucket.org/powerdesigninc/pdi-bitrise-database/utils"
	"github.com/bitrise-io/go-utils/colorstring"
)

type Flow struct{}

// create a change request on IT project
func (Flow) Do() error {
	// a lot of hard code since it can be only used in IT Project
	tasks := utils.ReadExecutedTasks()

	if len(tasks) == 0 {
		utils.PrintWarning("Skip create change request due to no executed tasks")

		return nil
	}

	var taskDescriptions = []map[string]interface{}{}

	for _, task := range tasks {
		issue, err := utils.GetJiraIssue(task)
		if err == nil {
			taskDescriptions = append(taskDescriptions, map[string]interface{}{
				"type": "listItem",
				"content": []map[string]interface{}{
					{
						"type": "paragraph",
						"content": []map[string]interface{}{
							{
								"type": "text",
								"text": issue.Id + ": " + issue.Title,
								"marks": []map[string]interface{}{
									{
										"type": "link",
										"attrs": map[string]interface{}{
											"href": issue.Url,
										},
									},
								},
							},
						}},
				},
			})
		}
	}

	var data = map[string]interface{}{
		"fields": map[string]interface{}{
			"project": map[string]interface{}{
				"id": "10189",
			},
			"issuetype": map[string]interface{}{
				"id": "10392",
			},
			"priority": map[string]interface{}{
				"id": "3",
			},
			"summary": fmt.Sprintf("[AD] %s: %s release", utils.GetRepoName(), time.Now().Format("02 Jan 2006")),
			"description": map[string]interface{}{
				"version": 1,
				"type":    "doc",
				"content": []map[string]interface{}{
					{
						"type": "paragraph",
						"content": []map[string]interface{}{
							{
								"type": "text",
								"text": "Build Number: ",
								"marks": []map[string]interface{}{
									{
										"type": "strong",
									},
								},
							}, {
								"type": "text",
								"text": utils.GetBuildNumber(),
							},
						},
					},
					{
						"type": "paragraph",
						"content": []map[string]interface{}{
							{
								"type": "text",
								"text": "Changes:",
								"marks": []map[string]interface{}{
									{
										"type": "strong",
									},
								},
							},
						},
					},
					{
						"type":    "bulletList",
						"content": taskDescriptions,
					},
				},
			},
			// Request Type
			"customfield_10010": "289", // Request a change
			// Implementation Steps
			"customfield_10068": map[string]interface{}{
				"version": 1,
				"type":    "doc",
				"content": []map[string]interface{}{
					{
						"type": "paragraph",
						"content": []map[string]interface{}{
							{
								"type": "text",
								"text": "Automated deployment",
							},
						},
					},
				},
			},
			// Impact
			"customfield_10004": map[string]interface{}{
				"id": "10003", // "Minor / Localized"
			},
			// Change reason
			"customfield_10007": map[string]interface{}{
				"id": "10013", // Maintenance
			},
		},
	}

	id, err := utils.CreateJiraIssue(data)

	if err == nil {
		fmt.Println(colorstring.Blue("Change Request Created, Id: ", id))
		return nil
	} else {
		utils.PrintWarning(err)
		return nil
	}
}
