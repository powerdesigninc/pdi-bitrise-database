package publishFlow

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"bitbucket.org/powerdesigninc/pdi-bitrise-database/history"
	"bitbucket.org/powerdesigninc/pdi-bitrise-database/utils"
	"github.com/bitrise-io/go-utils/colorstring"
)

type Flow struct{}

const confluenceAnchor = "<h2>Changelogs</h2>"

// publish by tag and update confluence page, and notify
func (Flow) Do() error {
	// update app setting
	var tag = utils.GetGitTag()
	var index = strings.Index(tag, "/")

	if index == -1 {
		utils.PrintWarning(tag + " is incorrect pattern")
		return nil
	}

	// test connection
	if err := utils.DbConnectionTest(); err != nil {
		return err
	}

	var env = tag[0:index]
	var version = tag[index+1:]

	if err := updateAppSettings(version); err != nil {
		return err
	}

	if env == "prod" {
		return prodPublish(version)
	}

	return nil
}

func prodPublish(version string) error {
	history, err := history.LoadUnpublishedInstall()
	if err != nil {
		return err
	}

	tasks := make([]*utils.JiraIssue, 0)
	for _, item := range history.Items {
		id := utils.FindJiraId(item.File)
		if id != "" {
			task, err := utils.GetJiraIssue(id)
			if err == nil {
				tasks = append(tasks, task)
			}
		}
	}

	sort.Slice(tasks, func(i, j int) bool {
		return tasks[i].Id < tasks[j].Id
	})

	page, err := updateConfluencePage(version, tasks)

	if err != nil {
		return err
	}

	notify(version, tasks, page)

	history.Publish()

	return history.Publish()
}

func updateAppSettings(version string) error {
	out, err := utils.ExecuteSqlOutput([]byte("SELECT TABLE_OWNER FROM ALL_SYNONYMS WHERE SYNONYM_NAME='WWV_FLOW_API';"))
	if err != nil {
		return err
	}

	apexUser := strings.Split(out, "\n")[3]

	appID := utils.GetDatabaseAppId()
	buildNumber := utils.GetBuildNumber()

	err = utils.ExecuteSqlE([]byte(fmt.Sprintf(`
		DECLARE
			l_workspace_id number;
			l_appId number := %s;
			l_appVersion varchar(100) := '%s';
			l_build_number number := %s;
			l_setting_count int;
		BEGIN
			SELECT
					workspace_id
			INTO l_workspace_id
			FROM
					apex_applications
			WHERE
					application_id = l_appId;

			apex_util.set_security_group_id (p_security_group_id => l_workspace_id);
			apex_application.g_flow_id := l_appId;

			SELECT
				COUNT(*)
			INTO l_setting_count
			FROM
				%v.wwv_flow_app_settings
			WHERE
				flow_id = l_appId
				AND name = 'APP_VERSION';

			IF l_setting_count = 0 THEN
				-- create
				wwv_flow_api.create_app_setting(p_name => 'APP_VERSION', p_value => l_appVersion, p_is_required => 'N');
				wwv_flow_api.create_app_setting(p_name => 'APP_BUILD_NUMBER', p_value => l_build_number, p_is_required => 'N');
			ELSE
				-- update
				apex_app_setting.set_value(p_name => 'APP_VERSION', p_value => l_appVersion);
				apex_app_setting.set_value(p_name => 'APP_BUILD_NUMBER', p_value => l_build_number);
			END IF;
		END;
		/
		COMMIT;
		`, appID, version, buildNumber, apexUser)))

	if err != nil {
		return err
	}

	fmt.Println(colorstring.Blue("Apex Setting has been updated"))

	return nil
}

func updateConfluencePage(version string, tasks []*utils.JiraIssue) (*utils.ConfluencePage, error) {
	page, err := utils.GetConfluencePage(utils.GetConfluencePageId())
	if err != nil {
		return nil, err
	}

	anchorIndex := strings.Index(page.Content, confluenceAnchor)
	if anchorIndex == -1 {
		utils.PrintWarning("Confluence page doesn't contain ", confluenceAnchor)
	} else {
		anchorIndex = anchorIndex + len(confluenceAnchor)
		builder := &strings.Builder{}
		builder.WriteString(page.Content[0:anchorIndex])

		// add new content after <h2>Changelogs</h2>
		fmt.Fprintf(builder, "<h3>v%v - %v</h3><ul>", version, time.Now().Format("Mon, 02 Jan 2006"))

		if len(tasks) == 0 {
			builder.WriteString("<li>None</li>")
		} else {
			for _, task := range tasks {
				fmt.Fprintf(builder, "<li><a href='%[1]v' data-card-appearance='inline'>%[1]v</a></li>", task.Url)
			}
		}

		builder.WriteString("</ul>")
		builder.WriteString(page.Content[anchorIndex:])

		page.Content = builder.String()

		err := page.Update()
		if err != nil {
			return nil, err
		}
	}

	fmt.Println(colorstring.Blue("Confluence page updated"))

	return page, nil
}

func notify(version string, tasks []*utils.JiraIssue, page *utils.ConfluencePage) {
	title := fmt.Sprintf("%v - v%v Has Been Released", utils.GetRepoName(), version)

	messageSections := []map[string]interface{}{
		{
			"activityTitle": title,
		},
	}

	if len(tasks) > 0 {
		builder := &strings.Builder{}
		for _, task := range tasks {
			fmt.Fprintf(builder, "- [%v: %v](%v)\n", task.Id, task.Title, task.Url)
		}

		messageSections = append(messageSections, map[string]interface{}{
			"text": "**Changeslogs**",
		}, map[string]interface{}{
			"text": builder.String(),
		})
	}

	message := map[string]interface{}{
		"@type":      "MessageCard",
		"summary":    title,
		"themeColor": "#10c289",
		"sections":   messageSections,
		"potentialAction": []map[string]interface{}{
			{
				"@type": "OpenUri",
				"name":  "Full Changelogs",
				"targets": []map[string]interface{}{
					{
						"os":  "default",
						"uri": page.Url,
					},
				},
			},
		},
	}

	for _, url := range utils.GetNotificationUrls() {
		utils.Request{
			Method: "POST",
			Headers: map[string]string{
				"Content-Type": "application/json",
			},
			Url:  url,
			Body: message,
		}.Do()
	}

	fmt.Println(colorstring.Blue("Message has been send"))
}
