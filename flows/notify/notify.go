package notifyFlow

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"bitbucket.org/powerdesigninc/pdi-bitrise-database/utils"
	utils_mail "bitbucket.org/powerdesigninc/pdi-bitrise-database/utils/mail"
	"github.com/bitrise-io/go-utils/colorstring"
)

type Flow struct{}

// notify build status
func (Flow) Do() error {
	isBuildSuccess := utils.IsBuildSuccess()
	buildState := utils.StringAB(utils.IsBuildSuccess(), "Success", "Fail")
	title := fmt.Sprintf("[%s]%s: %s", buildState, utils.GetRepoName(), utils.GetGitCommit())
	errorMessages := utils.ReadNotifyErrors()
	tasks := utils.ReadExecutedTasks()
	buildUrl := utils.GetBuildUrl()

	// for ms teams message
	urls := utils.GetNotificationUrls()
	if len(urls) == 0 {
		messageSections := []map[string]interface{}{
			{
				"activityTitle": title,
			},
			{
				"facts": []map[string]interface{}{
					{
						"name":  "Build Number",
						"value": utils.GetBuildNumber(),
					},
					{
						"name":  "Status",
						"value": buildState,
					},
					{
						"name":  "Environment",
						"value": utils.GetBuildId(),
					},
					{
						"name":  "Commit",
						"value": utils.GetGitCommit(),
					},
				},
			},
		}

		if len(errorMessages) > 0 {
			messageSections = append(messageSections, map[string]interface{}{
				"text": "**Error Messages**",
			}, map[string]interface{}{
				"text": "- " + strings.Join(errorMessages, "\n- "),
			})
		}

		if len(tasks) > 0 {
			builder := &strings.Builder{}
			for _, task := range tasks {
				issue, err := utils.GetJiraIssue(task)
				if err == nil {
					fmt.Fprintf(builder, "- [%v: %v](%v)\n", issue.Id, issue.Title, issue.Url)
				}
			}

			if builder.Len() > 0 {
				messageSections = append(messageSections, map[string]interface{}{
					"text": "**Changelogs**",
				}, map[string]interface{}{
					"text": builder.String(),
				})
			}
		}

		message := map[string]interface{}{
			"@type":      "MessageCard",
			"summary":    title,
			"themeColor": utils.StringAB(isBuildSuccess, "#10c289", "#ff2158"),
			"sections":   messageSections,
		}

		if buildUrl != "" {
			message["potentialAction"] = []map[string]interface{}{
				{
					"@type": "OpenUri",
					"name":  "Build Log",
					"targets": []map[string]interface{}{
						{
							"os":  "default",
							"uri": buildUrl,
						},
					},
				},
			}
		}

		for _, url := range urls {
			utils.Request{
				Method: "POST",
				Headers: map[string]string{
					"Content-Type": "application/json",
				},
				Url:  url,
				Body: message,
			}.Do()
		}
	}

	// for email, only send email when build fail or with error messages
	if !isBuildSuccess || len(errorMessages) > 0 {
		author := os.Getenv("GIT_CLONE_COMMIT_AUTHOR_EMAIL")
		commiter := os.Getenv("GIT_CLONE_COMMIT_COMMITER_EMAIL")

		to := []string{author}
		if commiter != author {
			to = append(to, commiter)
		}

		if isBuildSuccess && len(errorMessages) > 0 {
			title = fmt.Sprintf("[Warning]%s: %s", utils.GetRepoName(), utils.GetGitCommit())
		}

		body := &strings.Builder{}
		body.WriteString("<h3>Build Info</h3>\n")
		body.WriteString("<ul>\n")
		fmt.Fprintf(body, "<li><b>Build Number:</b> %s</li>\n", utils.GetBuildNumber())
		fmt.Fprintf(body, "<li><b>Status:</b> %s</li>\n", buildState)
		fmt.Fprintf(body, "<li><b>Environment:</b> %s</li>\n", utils.GetBuildId())
		fmt.Fprintf(body, "<li><b>Commit:</b> %s</li>\n", utils.GetGitCommit())
		body.WriteString("</ul>\n")

		if len(errorMessages) > 0 {
			body.WriteString("<h3>Error Messages</h3>\n")
			body.WriteString("<ul>\n")

			for _, m := range errorMessages {
				fmt.Fprintf(body, "<li>%s</li>\n", m)
			}

			body.WriteString("</ul>\n")
		}

		if buildUrl != "" {
			fmt.Fprintf(body, "<a href='%s'>Click here to see build detail</a>\n", buildUrl)
		}

		utils_mail.SendMail(&utils_mail.Mail{
			To:      to,
			Subject: title,
			Body:    body.String(),
		})
	}

	fmt.Println(colorstring.Blue("Message has been send"))

	if !isBuildSuccess || len(errorMessages) > 0 {
		return errors.New("build success with errors")
	}

	return nil
}
