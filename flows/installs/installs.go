package installsFlow

import (
	"fmt"
	"regexp"
	"strings"

	filesFlow "bitbucket.org/powerdesigninc/pdi-bitrise-database/flows/files"
	"bitbucket.org/powerdesigninc/pdi-bitrise-database/history"
	"bitbucket.org/powerdesigninc/pdi-bitrise-database/utils"
	"github.com/bitrise-io/go-utils/colorstring"
)

var includedFileRegex = regexp.MustCompile(`\@includeFile\((.+?)\)`)

type Flow struct{}

// execute sql files
func (Flow) Do() error {
	// list files
	var isProdBuild = utils.IsProdBuild()
	var installFolder string
	if isProdBuild {
		installFolder = "./00_installs/prod"
	} else {
		installFolder = "./00_installs/dev"
	}

	fmt.Println("Install Folder: " + installFolder)

	summary := make(map[string]bool)
	files, err := utils.ListSqlFiles(installFolder)
	if err != nil {
		return err
	}

	if len(files) == 0 {
		fmt.Println(colorstring.Yellowf("No file under %s", installFolder))
		return nil
	}

	// test connection
	if err := utils.DbConnectionTest(); err != nil {
		return err
	}

	// execute
	fmt.Println("Database Connection: " + utils.GetDatabaseConnectionString())
	fmt.Println("User/Schema: " + utils.GetDatabaseUser())

	var continueOnError = utils.IsContinueOnError()
	history, err := history.LoadInstallHistory(files)
	if err != nil {
		return err
	}

	var isFailed = false
	var hasIncludedFiles = false

	for _, file := range files {
		fileHash := file.RecurredSqlHash()
		if history.CheckHash(file.Filename, fileHash) {
			// file has been executed
			continue
		}

		fmt.Println("\n" + colorstring.Greenf("## Executing %v", file.Filename))

		output, err := utils.ExecuteSqlOutput(file.Bytes())

		println(output)

		if err == nil {
			// succeed

			// check if output includes special errors
			if strings.Contains(output, "compilation errors") ||
				strings.Contains(output, "unable to open file") ||
				strings.Contains(output, "insufficient privileges fail") {
				utils.WriteNotifyError("Non-ignorable errors in " + file.Filename)
			}

			summary[file.Filename] = true
			history.SetHash(file.Filename, fileHash)

			utils.WriteExecutedTask(file.Filename)

			// scan includedFiles
			for _, matches := range includedFileRegex.FindAllSubmatch(file.Bytes(), -1) {
				hasIncludedFiles = true
				match := strings.TrimSpace(string(matches[1]))
				// fmt.Println("Included File: " + match)
				utils.AddIncludedFile(match)
			}
		} else {
			// fail
			summary[file.Filename] = false
			utils.PrintError(err)
			utils.WriteNotifyError("Execution errors in " + file.Fullname)

			if !continueOnError {
				isFailed = true
				break
			}
		}
	}

	err = history.Save()
	if err != nil {
		return err
	}

	// summary
	fmt.Println("\n" + colorstring.Green("## SUMMARY"))
	for _, file := range files {
		status, ok := summary[file.Filename]
		if ok {
			if status {
				fmt.Printf("- %v : %v\n", file.Filename, colorstring.Green("Success"))
			} else {
				fmt.Printf("- %v : %v\n", file.Filename, colorstring.Red("Fail"))
			}
		} else {
			fmt.Printf("- %v : Skipped\n", file.Filename)
		}
	}

	if isFailed {
		return utils.ErrNoPrintError
	} else {
		if hasIncludedFiles {
			fmt.Println("\n" + colorstring.Green("## Uploading included files"))

			return filesFlow.Flow{}.Do()
		}

		return nil
	}
}
