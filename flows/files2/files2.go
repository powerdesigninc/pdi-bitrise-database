package files2Flow

import (
	"fmt"
	"regexp"
	"strings"

	filesFlow "bitbucket.org/powerdesigninc/pdi-bitrise-database/flows/files"
	"bitbucket.org/powerdesigninc/pdi-bitrise-database/utils"
	"github.com/bitrise-io/go-utils/colorstring"
)

var includedFileRegex = regexp.MustCompile(`\@includeFile\((.+?)\)`)

// include-based upload, scan includeFile in the install files and upload
type Flow struct{}

// execute sql files
func (Flow) Do() error {
	// list files
	var isProdBuild = utils.IsProdBuild()
	var installFolder string
	if isProdBuild {
		installFolder = "./00_installs/prod"
	} else {
		installFolder = "./00_installs/dev"
	}

	files, err := utils.ListSqlFiles(installFolder)
	if err != nil {
		return err
	}

	if len(files) == 0 {
		fmt.Println(colorstring.Yellowf("No file under %s", installFolder))
		return nil
	}

	var hasIncludedFiles = false

	for _, file := range files {
		// scan includeFile
		for _, matches := range includedFileRegex.FindAllSubmatch(file.Bytes(), -1) {
			hasIncludedFiles = true
			match := strings.TrimSpace(string(matches[1]))
			// fmt.Println("Included File: " + match)
			utils.AddIncludedFile(match)
		}
	}

	if hasIncludedFiles {
		fmt.Println("\n" + colorstring.Green("## Uploading included files"))

		return filesFlow.Flow{}.Do()
	}

	return nil
}
