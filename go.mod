module bitbucket.org/powerdesigninc/pdi-bitrise-database

go 1.15

require (
	github.com/Jeffail/gabs/v2 v2.6.1
	github.com/bitrise-io/go-utils v0.0.0-20210824130242-27933dca637a
)
