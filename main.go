package main

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"bitbucket.org/powerdesigninc/pdi-bitrise-database/flows"
	"bitbucket.org/powerdesigninc/pdi-bitrise-database/utils"
	"github.com/bitrise-io/go-utils/colorstring"
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			utils.PrintError(err)
			os.Exit(1)
		}
	}()

	// parse flows
	var inputFlows = os.Getenv("flows")
	var targetFlows []string
	if inputFlows == "" {
		targetFlows = []string{"installs"}
	} else {
		targetFlows = strings.Split(inputFlows, ",")
	}

	// execute flows
	for _, targetFlow := range targetFlows {
		targetFlow = strings.TrimSpace(strings.ToLower(targetFlow))
		flowFunc := flows.Flows[targetFlow]

		if flowFunc == nil {
			fmt.Println(colorstring.Red("# Unknown Flow: ") + colorstring.Green(targetFlow))
			os.Exit(1)
		} else {
			fmt.Println(colorstring.Green("# Executing " + targetFlow))
			err := flows.Flows[targetFlow].Do()
			if err != nil {
				if errors.Is(err, utils.ErrIgnorableError) {
					// stop with exit 0, for NotImportantError
					break
				} else {
					if !errors.Is(err, utils.ErrNoPrintError) {
						utils.PrintError(err)
					}

					os.Exit(1)
				}
			}

			fmt.Println()
		}
	}
}
