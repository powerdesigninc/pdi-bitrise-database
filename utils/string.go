package utils

import (
	"bytes"
	"math"
	"reflect"
	"sort"
	"text/template"
)

func ContainsString(arr []string, e string) bool {
	for _, a := range arr {
		if a == e {
			return true
		}
	}

	return false
}

func IndexOfString(arr []string, val string) int {
	for pos, v := range arr {
		if v == val {
			return pos
		}
	}

	return math.MaxInt32
}

func GetMapKeys(inputMap interface{}) []string {
	keys := []string{}

	v := reflect.ValueOf(inputMap)
	for _, kv := range v.MapKeys() {
		keys = append(keys, kv.String())
	}

	sort.Strings(keys)

	return keys
}

func StringAB(v bool, a string, b string) string {
	if v {
		return a
	} else {
		return b
	}
}

func FormatByMap(f string, m map[string]interface{}) (*bytes.Buffer, error) {
	var tpl bytes.Buffer
	t := template.Must(template.New("").Parse(f))
	if err := t.Execute(&tpl, m); err != nil {
		return nil, err
	}

	return &tpl, nil
}

func FormatByMapT(t *template.Template, m map[string]interface{}) (*bytes.Buffer, error) {
	var tpl bytes.Buffer
	if err := t.Execute(&tpl, m); err != nil {
		return nil, err
	}

	return &tpl, nil
}
