package utils

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/bitrise-io/go-utils/colorstring"
)

var ErrNoPrintError = errors.New("do not print")
var ErrIgnorableError = errors.New("do not fail")

var tempErrorFile = filepath.Join(os.TempDir(), "pdi_error.log")
var tempTaskFile = filepath.Join(os.TempDir(), "pdi_task.log")

func PrintError(err interface{}) {
	fmt.Println(colorstring.Redf("Error: %s", err))
}

func PrintWarning(a ...interface{}) {
	fmt.Println(colorstring.Yellow(a...))
}

func WriteNotifyError(str string) {
	file, err := os.OpenFile(tempErrorFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, os.ModePerm)
	if err == nil {
		file.WriteString(str + "\n")
	} else {
		PrintError(err)
	}
}

func ReadNotifyErrors() []string {
	return readTempFile(tempErrorFile)
}

func WriteExecutedTask(task string) {
	id := FindJiraId(task)
	if id != "" {
		file, err := os.OpenFile(tempTaskFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, os.ModePerm)
		if err == nil {
			file.WriteString(id + "\n")
		} else {
			PrintError(err)
		}
	}
}

func ReadExecutedTasks() []string {
	return readTempFile(tempTaskFile)
}

func readTempFile(file string) []string {
	tasks, err := ioutil.ReadFile(file)
	if err == nil {
		set := make(map[string]bool)

		for _, task := range strings.Split(string(tasks), "\n") {
			if task != "" {
				set[task] = true
			}
		}

		return GetMapKeys(set)
	} else {
		return nil
	}
}
