package utils

import (
	"os"
	"os/exec"
	"strings"
	"time"
)

func GitCheckout(branch string) error {
	cmd := exec.Command("git", "checkout", "--quiet", branch)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

func GitFetch() error {
	cmd := exec.Command("git", "fetch", "--quiet", "--unshallow")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

func GitMergedBranch(branch string) ([]string, error) {
	cmd := exec.Command("git", "branch", "--remotes", "--merged", branch)
	cmd.Stderr = os.Stderr

	output, err := cmd.Output()

	if err != nil {
		return nil, err
	}

	branches := make([]string, 0)
	for _, line := range strings.Split(string(output), "\n") {
		line = strings.TrimSpace(line)
		if line != "" {
			branches = append(branches, line)
		}
	}

	return branches, nil
}

func GitCommitDate(branch string) (time.Time, error) {
	cmd := exec.Command("git", "log", "-1", "--date=format:%Y-%m-%dT%TZ", "--format=%ad", branch)
	cmd.Stderr = os.Stderr
	output, err := cmd.Output()

	if err != nil {
		return time.Time{}, err
	}

	var s = strings.TrimSpace(string(output))
	return time.Parse(time.RFC3339, s)
}

func GitDeleteBranch(remote string, branch string) error {
	cmd := exec.Command("git", "push", remote, "--quiet", "--delete", branch)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}
