package utils_mail

import (
	"fmt"
	"net/smtp"
	"strings"

	"bitbucket.org/powerdesigninc/pdi-bitrise-database/utils"
)

type Mail struct {
	Subject string
	To      []string
	Body    string
}

type loginAuth struct {
	username, password string
}

func (s loginAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	return "LOGIN", []byte(s.username), nil
}

func (s loginAuth) Next(fromServer []byte, more bool) ([]byte, error) {
	if !more {
		return nil, nil
	}

	str := string(fromServer)

	switch str {
	case "Username:":
		return []byte(s.username), nil
	case "Password:":
		return []byte(s.password), nil
	default:
		return nil, fmt.Errorf("ErrUnknown: %s", str)
	}
}

const host = "smtp.office365.com:587"
const mime = "MIME-version: 1.0;\r\nContent-Type: text/html; charset=\"UTF-8\";\r\n"

func SendMail(m *Mail) {
	username := utils.GetSmtpUsername()
	password := utils.GetSmtpPassword()
	toStr := strings.Join(m.To, ",")

	auth := &loginAuth{username, password}

	msg := fmt.Sprintf("%sFrom: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n%s", mime, username, toStr, m.Subject, m.Body)

	err := smtp.SendMail(host, auth, username, m.To, []byte(msg))

	if err != nil {
		utils.PrintWarning("Send Email Failed, Error: ", err)
	}
}
