package utils

import (
	"crypto/sha256"
	"fmt"
	"io/ioutil"
	"mime"
	"os"
	"path/filepath"
	"regexp"
)

type FileInfo struct {
	// only filename
	Filename string
	// filename with path
	Fullname string

	Extension string
	mime      string
	bytes     []byte
	hash      string
}

var sqlFileRegex = regexp.MustCompile(`(?m)^@?@"(.+?)"`)
var excludesFiles = []string{"00_template.sql"}
var includesExts = []string{".sql"}

func (f *FileInfo) Bytes() []byte {
	if f.bytes == nil {
		var err error
		f.bytes, err = ioutil.ReadFile(f.Fullname)
		if err != nil {
			panic(err)
		}
	}

	return f.bytes
}

func (f *FileInfo) Hash() string {
	if f.hash == "" {
		f.hash = Hash(f.Bytes())
	}

	return f.hash
}

func (f *FileInfo) RecurredSqlHash() string {
	walker := make(map[string]string)

	recurredSqlHash(walker, f)

	hash := sha256.New()

	keys := GetMapKeys(walker)

	// fmt.Printf("[Debug] RecurredHash File: %v\n", f.Filename)
	for _, key := range keys {
		// fmt.Printf("[Debug] File: %v, Hash: %v\n", key, walker[key])
		fmt.Fprintf(hash, "%v:%v", key, walker[key])
	}

	// fmt.Printf("[Debug] Sum Hash: %X\n\n", hash.Sum(nil))
	return fmt.Sprintf("%X", hash.Sum(nil))
}

func (f *FileInfo) Mime() string {
	if f.mime == "" {
		f.mime = mime.TypeByExtension(f.Extension)
	}

	return f.mime
}

// list one layer sql files
func ListSqlFiles(path string) ([]*FileInfo, error) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}

	result := make([]*FileInfo, 0, len(files))

	for _, f := range files {
		if !f.IsDir() {
			ext := filepath.Ext(f.Name())

			if !ContainsString(includesExts, ext) {
				continue
			}

			if ContainsString(excludesFiles, f.Name()) {
				continue
			}

			result = append(result, &FileInfo{
				Filename:  f.Name(),
				Fullname:  filepath.Join(path, f.Name()),
				Extension: ext,
			})
		}
	}

	return result, nil
}

// list all file includes all children
func ListAllFiles(path string) ([]*FileInfo, error) {
	result := make([]*FileInfo, 0)

	err := filepath.Walk(path, func(fp string, f os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !f.IsDir() {
			result = append(result, &FileInfo{
				Filename:  f.Name(),
				Fullname:  fp,
				Extension: filepath.Ext(f.Name()),
			})
		}

		return nil
	})

	return result, err
}

func FolderHash(folder string) (string, error) {
	hash := sha256.New()

	err := filepath.Walk(folder, func(fp string, f os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !f.IsDir() {
			file := &FileInfo{
				Fullname: fp,
			}

			fmt.Fprintf(hash, "%v", file.Hash())
		}

		return nil
	})

	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%X", hash.Sum(nil)), nil
}

func recurredSqlHash(walker map[string]string, file *FileInfo) {
	_, ok := walker[file.Fullname]
	if ok {
		return
	}

	walker[file.Fullname] = file.Hash()

	for _, matches := range sqlFileRegex.FindAllSubmatch(file.Bytes(), -1) {
		recurredSqlHash(walker, &FileInfo{
			Fullname: filepath.Clean(string(matches[1])),
		})
	}
}
