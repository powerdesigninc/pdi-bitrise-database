package utils

import (
	"crypto/sha256"
	"fmt"
)

func Hash(data []byte) string {
	hash := sha256.New()

	hash.Write(data)

	return fmt.Sprintf("%X", hash.Sum(nil))
}
