package utils

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"time"

	"github.com/Jeffail/gabs/v2"
	"github.com/bitrise-io/go-utils/urlutil"
)

type Request struct {
	Method        string
	BaseUrl       string
	Url           string
	Headers       map[string]string
	Body          interface{}
	Authorization string
	Username      string
	Password      string
}

func (input Request) Do() ([]byte, error) {
	if IsDryRun() {
		return nil, nil
	}

	var reqBody io.Reader

	if input.Body != nil {
		var body []byte
		switch input.Body.(type) {
		case []byte:
			body = input.Body.([]byte)
		default:
			var err error
			body, err = json.Marshal(input.Body)
			if err != nil {
				return nil, err
			}
		}

		reqBody = bytes.NewBuffer(body)
	}

	var url string
	if input.BaseUrl == "" {
		url = input.Url
	} else {
		var err error
		url, err = urlutil.Join(input.BaseUrl, input.Url)
		if err != nil {
			return nil, err
		}
	}

	req, err := http.NewRequest(input.Method, url, reqBody)
	if err != nil {
		return nil, err
	}

	if input.Body != nil {
		req.Header.Add("Content-Type", "application/json")
	}

	if input.Username != "" && input.Password != "" {
		req.SetBasicAuth(input.Username, input.Password)
	}

	if input.Authorization != "" {
		req.Header.Add("Authorization", input.Authorization)
	}

	client := &http.Client{
		Timeout: time.Second * 10,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}

	res, err := client.Do(req)

	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	var resBody bytes.Buffer
	io.Copy(&resBody, res.Body)

	if res.StatusCode >= 400 {
		return nil, errors.New(resBody.String())
	}

	return resBody.Bytes(), nil
}

func (input Request) DoObject(output interface{}) error {
	bytes, err := input.Do()

	if err != nil {
		return err
	}

	err = json.Unmarshal(bytes, output)

	if err != nil {
		return err
	}

	return nil
}

func (input Request) DoJson() (*gabs.Container, error) {
	bytes, err := input.Do()

	if err != nil {
		return nil, err
	}

	return gabs.ParseJSON(bytes)
}
