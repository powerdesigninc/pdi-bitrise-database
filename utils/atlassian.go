package utils

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type JiraIssue struct {
	Id     string
	Title  string
	Url    string
	Status string
}

type JiraIssueTransition struct {
	Id   int
	Name string
}

type ConfluencePage struct {
	Id      string
	Type    string
	Title   string
	Url     string
	Version float64
	Content string
}

const jiraWebBaseUrl = "https://powerdesigninc.atlassian.net/browse/"
const jiraBaseUrl = "https://powerdesigninc.atlassian.net/rest/api/3/"
const confluenceBaseUrl = "https://powerdesigninc.atlassian.net/wiki/rest/api/content/"

var jiraIdRegex = regexp.MustCompile(`\w{1,6}[_-]\d{1,6}`)
var jiraIssueCache = make(map[string]*JiraIssue)

func FindJiraId(input string) string {
	id := jiraIdRegex.FindString(input)
	if id != "" {
		id = strings.ReplaceAll(id, "_", "-")
		id = strings.ToUpper(id)
	}

	return id
}

func CreateJiraIssue(data interface{}) (string, error) {
	var req = &Request{
		Method:   "POST",
		BaseUrl:  jiraBaseUrl,
		Url:      "/issue",
		Body:     data,
		Username: GetAtlassianUser(),
		Password: GetAtlassianPassword(),
	}

	json, err := req.DoJson()
	if err != nil {
		return "", errors.New("Create Jira Issue Failed, Error: " + err.Error())
	}

	return json.Path("key").Data().(string), nil
}

func GetJiraIssue(id string) (*JiraIssue, error) {
	jiraIssue, ok := jiraIssueCache[id]
	if ok {
		return jiraIssue, nil
	}

	var req = &Request{
		Method:   "GET",
		BaseUrl:  jiraBaseUrl,
		Url:      "/issue/" + id,
		Username: GetAtlassianUser(),
		Password: GetAtlassianPassword(),
	}

	json, err := req.DoJson()
	if err != nil {
		PrintWarning("JiraIssue: ", id, ", Error: ", err)
		return nil, err
	}

	jiraIssue = &JiraIssue{
		Id:     id,
		Title:  json.Path("fields.summary").Data().(string),
		Url:    jiraWebBaseUrl + id,
		Status: clearStatusName(strings.ToUpper(json.Path("fields.status.name").Data().(string))),
	}

	jiraIssueCache[id] = jiraIssue

	return jiraIssue, nil
}

func FindNextJiraIssueTransition(id string, nextStatus []string) (*JiraIssueTransition, error) {
	var req = &Request{
		Method:   "GET",
		BaseUrl:  jiraBaseUrl,
		Url:      "/issue/" + id + "/transitions",
		Username: GetAtlassianUser(),
		Password: GetAtlassianPassword(),
	}

	json, err := req.DoJson()
	if err != nil {
		return nil, err
	}

	transitions := json.Path("transitions").Children()

	if len(transitions) == 0 {
		PrintError(fmt.Sprintf("No permission to transition %v", id))
		return nil, errors.New("no permission")
	}

	for _, transition := range transitions {
		name := clearStatusName(strings.ToUpper(transition.Path("to.name").Data().(string)))
		transitionId, _ := strconv.Atoi(transition.Path("id").Data().(string))

		if ContainsString(nextStatus, name) {
			return &JiraIssueTransition{
				Id:   transitionId,
				Name: name,
			}, nil
		}
	}

	return nil, errors.New("no transition")
}

func UpdateJiraIssue(id string, transitionId int) error {
	if transitionId == -1 {
		return errors.New("no transition")
	} else {
		_, err := Request{
			Method:  "POST",
			BaseUrl: jiraBaseUrl,
			Url:     "/issue/" + id + "/transitions",
			Body: map[string]interface{}{
				"transition": map[string]interface{}{
					"id": transitionId,
				},
			},
			Username: GetAtlassianUser(),
			Password: GetAtlassianPassword(),
		}.Do()

		return err
	}
}

func GetConfluencePage(id string) (*ConfluencePage, error) {
	var req = &Request{
		Method:   "GET",
		BaseUrl:  confluenceBaseUrl,
		Url:      id + "?expand=body.editor,version",
		Username: GetAtlassianUser(),
		Password: GetAtlassianPassword(),
	}

	json, err := req.DoJson()

	if err != nil {
		return nil, err
	}

	link := json.Path("_links")

	return &ConfluencePage{
		Id:      id,
		Title:   json.Path("title").Data().(string),
		Type:    json.Path("type").Data().(string),
		Version: json.Path("version.number").Data().(float64),
		Url:     link.Path("base").Data().(string) + link.Path("webui").Data().(string),
		Content: json.Path("body.editor.value").Data().(string),
	}, nil
}

func (page *ConfluencePage) Update() error {
	body := map[string]interface{}{
		"type":  page.Type,
		"title": page.Title,
		"version": map[string]interface{}{
			"number": page.Version + 1,
		},
		"body": map[string]interface{}{
			"editor": map[string]interface{}{
				"value":          page.Content,
				"representation": "editor",
			},
		},
	}

	_, err := Request{
		Method:   "PUT",
		BaseUrl:  confluenceBaseUrl,
		Url:      page.Id,
		Body:     body,
		Username: GetAtlassianUser(),
		Password: GetAtlassianPassword(),
	}.Do()

	return err
}

func clearStatusName(name string) string {
	if strings.HasPrefix(name, "*") {
		return name[1:strings.LastIndex(name, " [")]
	} else {
		return name
	}
}
