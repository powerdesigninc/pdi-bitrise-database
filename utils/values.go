package utils

import (
	"fmt"
	"os"
	"strings"
)

var includedFiles = []string{}

func getEnv(name string, required bool) string {
	value := os.Getenv(name)

	if value == "" && required {
		panic(fmt.Sprintf("$%s is required", name))
	}

	return value
}

func IsDryRun() bool {
	return IsTrue(getEnv("DRY_RUN", false))
}

func GetEgnyteAccessToken() string {
	return "Bearer " + getEnv("EGNYTE_ACCESS_TOKEN", true)
}

func GetBuildId() string {
	return getEnv("BITRISE_TRIGGERED_WORKFLOW_ID", true)
}

func GetBuildTarget() string {
	if strings.Contains(strings.ToLower(GetBuildId()), "prod") {
		return "prod"
	} else {
		return "dev"
	}
}

func IsProdBuild() bool {
	return GetBuildTarget() == "prod"
}

func IsBuildSuccess() bool {
	return getEnv("BITRISE_BUILD_STATUS", false) == "0"
}

func GetRepoName() string {
	return getEnv("BITRISEIO_GIT_REPOSITORY_SLUG", true)
}

func GetGitTag() string {
	return getEnv("BITRISE_GIT_TAG", true)
}

func GetGitTagVersion() string {
	var tag = GetGitTag()
	var index = strings.Index(tag, "/")

	if index == -1 {
		return ""
	} else {
		return tag[index+1:]
	}
}

func GetGitCommit() string {
	return getEnv("GIT_CLONE_COMMIT_MESSAGE_SUBJECT", false)
}

func GetBuildNumber() string {
	return getEnv("BITRISE_BUILD_NUMBER", false)
}

func GetBuildUrl() string {
	return getEnv("BITRISE_BUILD_URL", false)
}

func GetAtlassianUser() string {
	return getEnv("ATLASSIAN_USER", true)
}

func GetAtlassianPassword() string {
	return getEnv("ATLASSIAN_PASSWORD", true)
}

func IsContinueOnError() bool {
	return IsTrue(getEnv("continue_on_error", false))
}

func IsContinueOnNoConnectionError() bool {
	return IsTrue(getEnv("continue_on_connection_error", false))
}

func GetDatabaseConnectionString() string {
	return getEnv("db_conn_str", true)
}

func GetDatabaseUser() string {
	return getEnv("db_user", true)
}

func GetDatabasePassword() string {
	return getEnv("db_password", true)
}

func GetDatabaseAppId() string {
	return getEnv("db_app_id", true)
}

func GetConfluencePageId() string {
	return getEnv("confluence_page_id", true)
}

func GetCleanBranch() string {
	var branch = getEnv("CLEAN_TARGET_BRANCH", false)
	if branch == "" {
		return "dev"
	} else {
		return branch
	}
}

func GetNotificationUrls() []string {
	return strings.Split(getEnv("notification_webhooks", false), ",")
}

func GetGhostInspectorApiKey() string {
	return getEnv("GHOSTINSPECTOR_API_KEY", true)
}

func GetSmtpUsername() string {
	return getEnv("SMTP_USERNAME", true)
}

func GetSmtpPassword() string {
	return getEnv("SMTP_PASSWORD", true)
}

func GetIncludedFiles() []string {
	var value = getEnv("FILES", false)
	if value == "" {
		return includedFiles
	} else {
		return append(includedFiles, strings.Split(strings.ToLower(value), ",")...)
	}
}

func AddIncludedFile(file string) {
	includedFiles = append(includedFiles, strings.ToLower(file))
}

func IsTrue(value string) bool {
	return value == "1" || value == "true"
}


