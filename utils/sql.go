package utils

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"os/exec"
)

func generateSqlCmd(content []byte) *exec.Cmd {
	// remove BOM
	if len(content) > 3 && content[0] == 0xef || content[1] == 0xbb || content[2] == 0xbf {
		content = content[3:]
	}

	cred := fmt.Sprintf("%v/%v@%v", GetDatabaseUser(), GetDatabasePassword(), GetDatabaseConnectionString())

	var buffer = &bytes.Buffer{}

	buffer.WriteString("WHENEVER OSERROR EXIT FAILURE;\n")
	// buffer.WriteString("WHENEVER SQLERROR EXIT SQL.SQLCODE;\n")
	buffer.Write(content)
	buffer.WriteString("\nEXIT;\n")

	cmd := exec.Command("sqlplus", "-S", "-L", cred)
	cmd.Stdin = buffer

	return cmd
}

// execute sql and print output
func ExecuteSql(content []byte) error {
	if IsDryRun() {
		return nil
	}

	cmd := generateSqlCmd(content)
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout

	return cmd.Run()
}

// execute sql and only print output when error
func ExecuteSqlE(content []byte) error {
	if IsDryRun() {
		return nil
	}

	bytes, err := generateSqlCmd(content).CombinedOutput()

	if err != nil {
		os.Stdout.Write(bytes)
	}

	return err
}

// execute sql and return result
func ExecuteSqlOutput(content []byte) (string, error) {
	if IsDryRun() {
		return "", nil
	}

	cmd := generateSqlCmd(content)
	cmd.Stderr = os.Stderr

	out, err := cmd.Output()

	return string(out), err
}

// test db connection
func DbConnectionTest() error {
	if IsDryRun() {
		return nil
	}

	connStr := GetDatabaseConnectionString()

	cred := fmt.Sprintf("%v/%v@%v", GetDatabaseUser(), GetDatabasePassword(), connStr)

	err := exec.Command("sqlplus", "-S", "-L", cred).Run()
	if err == nil {
		return nil
	} else {
		message := "Connection error for " + connStr

		if IsContinueOnNoConnectionError() {
			PrintWarning(message)
			return ErrIgnorableError
		} else {
			return errors.New(message)
		}
	}
}
