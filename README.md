# PDI Bitrise Setup

This project is our costume step for bitrise that help us simplified establishing building environments.

## Usage

Unfortunately, Bitrise Workflow Editor doesn't support editing private step when the script was written. we have to use bitrise.yml editor to add the step. The below is a snippet of adding the step into bitrise.yml

```yaml
steps:
  - git::https://bitbucket.org/powerdesigninc/pdi-bitrise-database.git@main:
      title: Execute Database Deployment
      inputs:
        # the values are default values,
        # if you put the values in env, then you can skip to set them
        - flows: $DB_FLOWS # $DB_FLOWS is empty then, the default is installs
        - install_sql: '00_install.sql'
        - db_user: $DB_USER
        - db_password: $DB_PASSWORD
        - db_conn_str: $DB_CONN_STR
        - db_app_id: $DB_APP_ID
        - continue_on_error: $CONTINUE_ON_ERROR # 1 or true
        # [deprecated] below options are for file_sync flow
        - sync_config: '00_file_sync.yml'
        - db_user_src: $DB_USER_SRC
        - db_password_src: $DB_PASSWORD_SRC
        - db_conn_str_src: $DB_CONN_STR_SRC
        - db_app_id_src: $DB_APP_ID_SRC
```

## Environment Variables

Database related steps provides some environment variables already, so you can skip to define them on bitrise

| Name                | Description                               |
| ------------------- | ----------------------------------------- |
| DB_CONN_SRT_DEV     | the connection str for dev db cluster     |
| DB_CONN_SRT_TEST    | the connection str for test db cluster    |
| DB_CONN_SRT_UAT     | the connection str for uat db cluster     |
| DB_CONN_SRT_PROD    | the connection str for prod db cluster    |
| DB_PASSWORD_NONPROD | the database password for prod db cluster |

- **To know complete inputs and their default values, see [step.yml](./step.yml).**
  `@main` can be replaced to a branch or tag. main is the production branch and use it, the workflow will always use latest version, but you can switch to old version if you want to.

## Flows

These are the supported flows

| Name      | Description                                                                                          |
| --------- | ---------------------------------------------------------------------------------------------------- |
| installs  | execute sql scripts under `00_installs` folders, the same content sql file can be only executed once |
| install   | execute 00_install.sql for sql executed every deployment                                             |
| files     | upload files under `files` folder to Apex Application Static Files                                   |
| old       | [deprecated] execute 00_install.sql and 00_rollback.sql if fails and rollback to last 00_install.sql |
| file_sync | [deprecated] sync file based on 00_file_sync.yml                                                     |

### installs

Executing sql scripts under `00_installs` folders, each sql can be executed only once

1. put sql in 00_installs folder or a subfolder, the file name is recommended to `<jira_id>.sql` or `<jira_id>/<name>.sql`
2. the execute order is by file name that is matched order in file explorer or finder with sorting by name, for example,

   ```
   # file structure
   00_installs
   ├── 001.sql
   ├── 002.sql
   ├── 003
   │   ├── 001.sql
   │   └── 002.sql
   └── 004.sql

   # execute order
   - 001.sql
   - 002.sql
   - 003/001.sql
   - 003/002.sql
   - 004.sql
   ```

3. a sql file can be only executed one time if the content isn't changed,

> the yaml execution history is store on Egnyte `/Shared/PDI%20Web%20Server/external/cdn/database-deploy-histories/${conn}-${user}/${repo}/installs.yml`;

### install

execute 00_install.sql for sql executed every deployment

1. put the sql script in the 00_install.sql files
2. the file will be executed when deploying

> install flow is better for hotfix or some things need to be execute every deployments. For regular deployment, we recommend to use installs flow which has less commit conflict and better commit history

### files

upload files under `files` folder to Apex Application Static

1. put any static files such as image, js, css or docs under `files` folder.
2. only new files will be uploaded to apex application static files
3. a path of a file will be matched to apex static files for example, a file `<repo root>/files/scripts/my.js` will be uploaded as `scripts/my.js` on Apex.
4. each file will be hashed to

> upload history is store on Egnyte `/Shared/PDI%20Web%20Server/external/cdn/database-deploy-histories/${conn}-${user}/${repo}/files.yml`;

# Test Locally

Bitrise runs the workflow in docker with its images like `bitriseio/docker-bitrise-base`. We can do the same thing to emulator its running environment.

```bash
docker run --rm -v $PWD:/bitrise-database -v $PWD/bitrise.yml:/bitrise/src/bitrise.yml -e EGNYTE_ACCESS_TOKEN=$EGNYTE_ACCESS_TOKEN bitriseio/docker-bitrise-base bitrise run test
```
